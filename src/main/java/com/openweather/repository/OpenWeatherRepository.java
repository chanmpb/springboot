package com.openweather.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.openweather.entity.OpenWeatherEntity;

@Repository
public interface OpenWeatherRepository extends JpaRepository<OpenWeatherEntity, Long>
{
    public OpenWeatherEntity findFirstByOrderByIdAsc();
}