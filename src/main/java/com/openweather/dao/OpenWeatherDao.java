package com.openweather.dao;

import com.openweather.model.OpenWeather;

public interface OpenWeatherDao
{
    public void saveWeather(OpenWeather openWeatherMap);

    public long countWeatherData();
    
    public void deleteOldestEntry();
}
