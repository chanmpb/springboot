package com.openweather.dao.impl;

import com.openweather.dao.OpenWeatherDao;
import com.openweather.entity.OpenWeatherEntity;
import com.openweather.model.OpenWeather;
import com.openweather.repository.OpenWeatherRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.stereotype.Component;

@Component
public class OpenWeatherDaoImpl implements OpenWeatherDao
{
    @Autowired
    OpenWeatherRepository openWeatherRepository;

    @Override
    public void saveWeather(OpenWeather openWeather)
    {
        OpenWeatherEntity entity = convertModelToEntity(openWeather);
        openWeatherRepository.save(entity);
    }

    @Override
    public long countWeatherData()
    {
        return openWeatherRepository.count();
    }

    @Override
    public void deleteOldestEntry()
    {
        Long id = openWeatherRepository.findFirstByOrderByIdAsc().getId();
        openWeatherRepository.deleteById(id);
    }

    private OpenWeatherEntity convertModelToEntity(OpenWeather openWeather)
    {
        OpenWeatherEntity entity = new OpenWeatherEntity();
        entity.setResponseId(UUID.randomUUID().toString());
        entity.setLocation(openWeather.getName());
        entity.setActualWeather(openWeather.getWeather().get(0).getDescription());
        entity.setTemperature(openWeather.getMain().getTemp());
        entity.setDtimeInserted(LocalDateTime.now());
        return entity;
    }
}