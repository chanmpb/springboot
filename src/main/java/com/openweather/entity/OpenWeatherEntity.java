package com.openweather.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Table;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Table(name = "weatherlog", schema = "openweather")
public class OpenWeatherEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String responseId;

    private String location;

    private String actualWeather;

    private String temperature;

    private LocalDateTime dtimeInserted;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getResponseId()
    {
        return responseId;
    }

    public void setResponseId(String responseId)
    {
        this.responseId = responseId;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getActualWeather()
    {
        return actualWeather;
    }

    public void setActualWeather(String actualWeather)
    {
        this.actualWeather = actualWeather;
    }

    public String getTemperature()
    {
        return temperature;
    }

    public void setTemperature(String temperature)
    {
        this.temperature = temperature;
    }

    public LocalDateTime getDtimeInserted()
    {
        return dtimeInserted;
    }

    public void setDtimeInserted(LocalDateTime dtimeInserted)
    {
        this.dtimeInserted = dtimeInserted;
    }
}
