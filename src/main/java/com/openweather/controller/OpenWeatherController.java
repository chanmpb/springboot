package com.openweather.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;

import com.openweather.model.OpenWeather;
import com.openweather.service.OpenWeatherService;

@RestController
public class OpenWeatherController
{
    private final Logger logger = LoggerFactory.getLogger(OpenWeatherController.class);

    @Autowired
    OpenWeatherService openWeatherService;

    @RequestMapping(value = "/weather/{location}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OpenWeather> getWeather(@PathVariable String location)
    {
        ResponseEntity<OpenWeather> response = null;

        try
        {
            logger.info(String.format("----- START OpenWeatherController.getWeather(%s)----- ", location));
            response = new ResponseEntity<>(openWeatherService.getWeather(location), HttpStatus.OK);
            logger.info(String.format("----- END OpenWeatherController.getWeather(%s)----- ", location));
        } catch (RestClientException restClientException)
        {
            logger.error(String.format("----- ERROR: %s -----", restClientException));
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return response;
    }

    @RequestMapping(value = "/weather", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<OpenWeather>> getAllWeather()
    {
        ResponseEntity<List<OpenWeather>> response = null;

        try
        {
            logger.info("----- START OpenWeatherController.getAllWeather----- ");
            response = new ResponseEntity<>(openWeatherService.getAllWeather(), HttpStatus.OK);
            logger.info("----- END OpenWeatherController.getAllWeather----- ");
        } catch (RestClientException restClientException)
        {
            logger.error(String.format("----- ERROR: %s -----", restClientException));
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return response;
    }

    @RequestMapping(value = "/ping")
    public String ping()
    {
        return "Open Weather Map responding successfully!";
    }
}