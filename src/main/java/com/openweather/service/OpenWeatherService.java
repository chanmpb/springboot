package com.openweather.service;

import java.util.List;

import com.openweather.model.OpenWeather;

public interface OpenWeatherService
{
    public OpenWeather getWeather(String location);

    public List<OpenWeather> getAllWeather();
}
