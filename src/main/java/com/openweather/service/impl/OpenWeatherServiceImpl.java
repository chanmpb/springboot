package com.openweather.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.openweather.dao.OpenWeatherDao;
import com.openweather.model.OpenWeather;
import com.openweather.service.OpenWeatherService;

@Component
public class OpenWeatherServiceImpl implements OpenWeatherService
{
    private final Logger logger = LoggerFactory.getLogger(OpenWeatherServiceImpl.class);
    private final String OPENWEATHER_URL = "https://api.openweathermap.org/data/2.5/weather?q=%s&APPID=fa05fdaefd1796aba1d28a925f588b91";
    private static final int MAX_DATA = 5;
    private static final String LONDON = "London";
    private static final String PRAGUE = "Prague";
    private static final String SAN_FRANCISCO = "San Francisco";

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    OpenWeatherDao openWeatherDao;

    @Override
    public OpenWeather getWeather(String location)
    {
        OpenWeather openWeather = restTemplate.getForEntity(String.format(OPENWEATHER_URL, location), OpenWeather.class).getBody();

        logger.info(String.format("----- START OpenWeatherServiceImpl.saveWeather(%s)----- ", location));

        if (openWeatherDao.countWeatherData() >= MAX_DATA)
        {
            openWeatherDao.deleteOldestEntry();
        }

        openWeatherDao.saveWeather(openWeather);

        logger.info(String.format("----- START OpenWeatherServiceImpl.saveWeather(%s)----- ", location));

        return openWeather;
    }

    @Override
    public List<OpenWeather> getAllWeather()
    {
        List<String> locations = new ArrayList<>(Arrays.asList(LONDON, PRAGUE, SAN_FRANCISCO));
        List<OpenWeather> weatherList = new ArrayList<>();

        locations.forEach(location -> weatherList.add(getWeather(location)));

        return weatherList;
    }
}