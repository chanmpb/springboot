package com.openweather.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class OpenWeather implements Serializable
{
    private static final long serialVersionUID = 1L;

    Coord coord;
    List<Weather> weather;
    String base;
    Main main;
    String visibility;
    Wind wind;
    Map<String, String> rain;
    Clouds clouds;
    String dt;
    Sys sys;
    String timezone;
    String id;
    String name;
    String cod;

    public Coord getCoord()
    {
        return coord;
    }

    public void setCoord(Coord coord)
    {
        this.coord = coord;
    }

    public List<Weather> getWeather()
    {
        return weather;
    }

    public void setWeather(List<Weather> weather)
    {
        this.weather = weather;
    }

    public String getBase()
    {
        return base;
    }

    public void setBase(String base)
    {
        this.base = base;
    }

    public Main getMain()
    {
        return main;
    }

    public void setMain(Main main)
    {
        this.main = main;
    }

    public String getVisibility()
    {
        return visibility;
    }

    public void setVisibility(String visibility)
    {
        this.visibility = visibility;
    }

    public Wind getWind()
    {
        return wind;
    }

    public void setWind(Wind wind)
    {
        this.wind = wind;
    }

    public Map<String, String> getRain()
    {
        return rain;
    }

    public void setRain(Map<String, String> rain)
    {
        this.rain = rain;
    }

    public Clouds getClouds()
    {
        return clouds;
    }

    public void setClouds(Clouds clouds)
    {
        this.clouds = clouds;
    }

    public String getDt()
    {
        return dt;
    }

    public void setDt(String dt)
    {
        this.dt = dt;
    }

    public Sys getSys()
    {
        return sys;
    }

    public void setSys(Sys sys)
    {
        this.sys = sys;
    }

    public String getTimezone()
    {
        return timezone;
    }

    public void setTimezone(String timezone)
    {
        this.timezone = timezone;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCod()
    {
        return cod;
    }

    public void setCod(String cod)
    {
        this.cod = cod;
    }

}
