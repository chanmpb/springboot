package com.openweather.model;

import java.io.Serializable;

public class Clouds implements Serializable
{
    private static final long serialVersionUID = 1L;

    String all;

    public String getAll()
    {
        return all;
    }

    public void setAll(String all)
    {
        this.all = all;
    }
}
