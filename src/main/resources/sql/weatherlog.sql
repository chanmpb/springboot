  CREATE TABLE openweather.weatherlog (
  `id` INT NOT NULL AUTO_INCREMENT,
  `responseId` VARCHAR(255),
  `location` VARCHAR(255),
  `actualWeather` VARCHAR(255),
  `temperature` VARCHAR(255),
  `dtimeInserted` DATETIME,
  PRIMARY KEY (`id`));